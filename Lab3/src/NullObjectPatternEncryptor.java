/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

/**
 * Class for providing an encryptor that does not alter the plaintext
 */
public class NullObjectPatternEncryptor implements Encryptor {

    /**
     * "Encrypts" the plaintext as plaintext
     * @param msg the message to "encrypt"
     * @return the "encrypted" message
     */
    @Override
    public byte[] encrypt(byte[] msg) {
        return msg;
    }

    /**
     * "Decrypts" the plaintext from plaintext
     * @param msg the message to "decrypt"
     * @return the "decrypted" message
     */
    @Override
    public byte[] decrypt(byte[] msg) {
        return msg;
    }
}
