/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

/**
 * A class for encrypting messages by reversing them
 */
public class ReverseEncryptor implements Encryptor {

    /**
     * Method for encrypting a message by reversing it
     * @param msg the message to encrypt
     * @return the reversed message
     */
    @Override
    public byte[] encrypt(byte[] msg) {
        // Instantiate an array to save the reversed message
        byte[] revArray = new byte[msg.length];

        // For each letter in the message, going backwards, append it to revArray
        for (int i = msg.length - 1; i > -1; i--){
            revArray[msg.length - (i + 1)] = msg[i];
        }

        return revArray;
    }

    /**
     * Method for decrypting a message by reversing it
     * @param msg the message to decrypt
     * @return the reversed message
     */
    @Override
    public byte[] decrypt(byte[] msg) {
        return encrypt(msg);    // Since reverse is symmetric, encrypting twice equals the plaintext
    }
}
