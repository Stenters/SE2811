/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

/**
 * Class for encrypting a message by shifting each character by a defined amount
 */
public class ShiftEncryptor implements Encryptor {
    private int amount;

    /**
     * Constructor for the encryptor, defines how much to shift a character
     * @param index the amount to shift each character
     */
    public ShiftEncryptor(int index) {
        amount = index;
    }

    /**
     * Method for encrypting a message by shifting each character
     * @param msg the message to encrypt
     * @return the encrypted message
     */
    @Override
    public byte[] encrypt(byte[] msg) {
        // For each character, shift it. If it's greater then 256, wrap around to 0 and continue
        for(int i = 0; i < msg.length; i++){
            msg[i] = (byte) ((msg[i] + amount) % 256);
        }

        return msg;
    }

    /**
     * Method for decrypting a message by shifting each character
     * @param msg the message to decrypt
     * @return the decrypted message
     */
    @Override
    public byte[] decrypt(byte[] msg) {
        // Negate the amount, then shift
        amount = -amount;
        msg = encrypt(msg);
        amount = -amount;
        return msg;
    }

}
