/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Class for storing encrypted messages
 */
public class Media {
    private byte[] bytes;

    /**
     * Method for setting the message given a byte array
     * @param msg the encrypted message
     */
    public void set(byte[] msg){
        this.bytes = msg;
        write();
    }

    /**
     * Method for converting a string containing space separated hex pairs into a byte array
     * and setting that to the message
     * @param msg the message to convert and save
     */
    public void set(String msg){
        // Eliminate trailing whitespace
        while (msg.endsWith(" ") || msg.endsWith("\n")){
            msg = msg.substring(0, msg.length() - 1);
        }

        // Split the string into array of hex pairs, initialize the byte array
        String[] chars = msg.split(" ");
        bytes = new byte[chars.length];

        // For each hex pair, convert them into bytes and save them
        for (int i = 0; i < chars.length; i++) {
            bytes[i] = (byte) (Integer.parseInt(chars[i], 16));
        }

        // Write the result to file
        write();
    }

    /**
     * Method for defining how to write the byte array to a string
     * @return the byte array as a string
     */
    @Override
    public String toString(){
        StringBuilder hexBuilder = new StringBuilder();

        for (byte b : bytes) {
            // If the byte is one digit, append a 0 to the start of it
            if (-1 < b && b < 16){
                hexBuilder.append("0");
            }

            // Append the byte to the string in hexadecimal and separate with a space
            hexBuilder.append(Integer.toHexString(b & 0xFF));
            hexBuilder.append(" ");
        }

        // Remove the last space
        hexBuilder.deleteCharAt(hexBuilder.length() - 1);
        return hexBuilder.toString();
    }

    /**
     * Helper method for writing to a file
     */
    private void write() {
        // Try to create a print writer and print the encrypted message
        try (PrintWriter output = new PrintWriter(System.getProperty("user.dir") + "\\Media.txt")){
            output.print(toString());
            output.flush();

        } catch (IOException e){
            System.err.println("Error writing cyphertext to file");
        }
    }
}
