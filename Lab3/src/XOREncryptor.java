/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

/**
 * Class for encrypting a message by XORing each character against a character in a key
 */
public class XOREncryptor implements Encryptor {
    private byte[] key;

    /**
     * Constructor for the XOR encrypter to set the key
     * @param key the key to encrypt with
     */
    public XOREncryptor(String key) {
        // Turn the key to an array of characters, then append them to a byte array
        char[] charKey = key.toCharArray();
        this.key = new byte[charKey.length];

        for (int i = 0; i < charKey.length; i++) {
            this.key[i] = (byte) charKey[i];
        }
    }

    /**
     * Method for encrypting a message by XORing each character against a key
     * @param msg the message to encrypt
     * @return the encrypted message
     */
    @Override
    public byte[] encrypt(byte[] msg) {
        // For every character in the message and key, XOR them together and save the result
        for (int i = 0; i < msg.length; i++){
            int j = i % key.length;

            msg[i] = (byte) (msg[i] ^ key[j]);

        }

        return msg;
    }

    /**
     * Method for dencrypting a message by XORing each character against a key
     * @param msg the message to decrypt
     * @return the decrypted message
     */
    @Override
    public byte[] decrypt(byte[] msg) {
        return encrypt(msg);    // Since XOR is symmetric, encrypting twice equals the plaintext
    }
}
