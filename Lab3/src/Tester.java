/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

import java.io.*;
import java.util.Scanner;

/**
 * Class for testing the Encryptors given sample files containing input and output of successful runs labeled
 * in.* and out.* where * is a count from 1 through the number of tests
 */
public class Tester {

    /**
     * Constructor / tester method
     */
    Tester(){

        try {
            // Query the user for the test to run
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please specify a test to run (1..8)");
            int i = scanner.nextInt();

            // Set System.in and System.out to desired files, and run the app
            System.err.println(">>> Beginning tests");
            FileInputStream input = new FileInputStream(new File(System.getProperty("user.dir") + "\\lab3_tests\\in." + i));
            PrintStream output = new PrintStream(new File("C:\\Projects\\se2811\\Lab3\\lab3_tests\\sout." + i));

            System.setIn(input);
            System.setOut(output);
            Main.getInput();
            output.flush(); // Make sure the output actually writes to file

            // Connect scanners to the actual output file and the expected output file,
            // then read the contents into a String array separated by new line (\n) characters
            Scanner expectedReader = new Scanner(new File(System.getProperty("user.dir") + "\\lab3_tests\\out." + i));
            Scanner actualReader = new Scanner(new File(System.getProperty("user.dir") + "\\lab3_tests\\sout." + i));

            StringBuilder temp = new StringBuilder();
            while(expectedReader.hasNext()){
                temp.append(expectedReader.next());
            }
            String[] expected = temp.toString().split("\n");

            temp = new StringBuilder(); // Clear the StringBuilder

            while(actualReader.hasNext()){
                temp.append(actualReader.next());
            }
            String[] actual = temp.toString().split("\n");

            // Iterate through the lines of the String arrays an check that they are equivalent
            for (int j = 0; j < actual.length; j++) {
                if (!expected[j].equals(actual[j])){
                    throw new IndexOutOfBoundsException("The test failed on line " + j);
                } else{
                    System.err.println("\tPassed test on line " + j);
                }
            }

            // Close file I/O objects
            input.close();
            output.close();
            expectedReader.close();
            actualReader.close();

            // Expected and actual output were identical
            System.err.println(">>> All tests passed");

        } catch (FileNotFoundException e){  // If the file I/O objects failed to hook up
            System.err.println("The test files were relocated");

        } catch (IOException e){    // If there was an unexpected error while reading or writing to file
            System.err.println("Unexpected error while reading from test file");

        } catch (IndexOutOfBoundsException e){  // If a test failed (or files were different lengths (which should fail))
            System.err.println(e.getMessage());
        }

    }
}