/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

import java.util.Scanner;

/**
 * Class for interacting with the user to encrypt or decrypt data
 */
public class Main {
    private static CryptStick msg;

    /**
     * Start of the application
     * @param args unused
     */
    public static void main(String[] args) {
        // Create a new CryptStick and encrypt or decrypt the message
        msg = new CryptStick();
        getInput();
    }

    /**
     * Method for interfacing with the user
     */
    public static void getInput() {
        // Query the user if they would like to encrypt or decrypt data
        Scanner input = new Scanner(System.in);
        String response = getResponse("E)ncrypt or D)ecrypt: ", input);

        switch (response) {
            case "d": {

                // Get the strategy to decrypt with
                response = getResponse("Method (rev, shift, xor): ", input);
                getCipherStrategy(input, response);

                // Get the encrypted message and save it to the encryptor
                response = getFullResponse(input);
                msg.getMedia().set(response);

                // Print out the cleartext
                System.out.println(msg.getMessage());
                break;

            }
            case "e": {

                // Get the strategy to encrypt with
                response = getResponse("Method (rev, shift, xor): ", input);
                getCipherStrategy(input, response);

                // Get the message the user wishes to encrypt
                response = getFullResponse(input);
                msg.setMessage(response);
                break;

            }
            case "t": {

                // Test the program
                new Tester();
                break;
            }
            default:

                // The user entered an invalid input, try again
                System.out.println("Please enter a valid input");
                getInput();
                break;

        }
    }

    /**
     * Method for getting the strategy the user wishes to use
     * @param input the input to read from
     * @param strat the strategy the user desires
     */
    private static void getCipherStrategy(Scanner input, String strat) {
        try {
            // Initialize the container for the users response
            String response;
            switch (strat) {
                case "xor":

                    // Query for the key to apply to the XOREncrypter
                    System.out.print("Key: ");
                    response = input.nextLine();
                    msg.setEncryptionStrategy(new XOREncryptor(response));
                    break;

                case "shift":

                    // Query for the shift amount to apply to the ShiftEncryptor
                    response = getResponse("Shift amount: ", input);
                    msg.setEncryptionStrategy(new ShiftEncryptor(Integer.parseInt(response)));
                    break;

                case "rev":

                    // Nothing extra needs to pe done
                    msg.setEncryptionStrategy(new ReverseEncryptor());
                    break;

                case "none":

                    // Encrypt and Decrypt do nothing
                    msg.setEncryptionStrategy(new NullObjectPatternEncryptor());
                    break;

                default:

                    // Invalid input entered, try again
                    getCipherStrategy(input, getResponse("Please enter a valid method", input));
            }

        } catch (NumberFormatException e){

            // If the shift amount isn't a valid number, try again
            System.out.println("Please enter a valid index");
            getCipherStrategy(input, strat);
        }
    }

    /**
     * Helper method for interfacing with the user
     * @param msg the message to tell the user
     * @param input the input that contains the response
     * @return the response of the user
     */
    private static String getResponse(String msg, Scanner input) {
        System.out.print(msg);
        return input.nextLine().toLowerCase();
    }

    /**
     * Helper method for getting a EOF terminated message from the user
     * @param input the input that contains the response
     * @return the response of the user
     */
    private static String getFullResponse(Scanner input) {
        System.out.println("Message: ");
        StringBuilder sb = new StringBuilder();
        while (input.hasNextLine()){
            sb.append(input.nextLine());
            sb.append("\n");
        }
        return sb.toString();
    }

}
