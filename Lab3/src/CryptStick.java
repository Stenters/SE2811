/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

import java.nio.charset.StandardCharsets;

/**
 * Class for handling encrypting or decrypting data, as well as storing that data in a Media object
 */
public class CryptStick {

    private Media media;
    private Encryptor encryptor;

    /**
     * Helper method for getting the CryptStick's Media object
     * @return the CryptStick's Media object
     */
    protected Media getMedia() {
        // If the media object hasn't been instantiated yet, create it
        if (media == null){
            media = new Media();
        }

        return media;
    }

    /**
     * Method for setting the encryption strategy
     * @param encryptionStrategy the encryption strategy to use
     */
    public void setEncryptionStrategy(Encryptor encryptionStrategy){
        this.encryptor = encryptionStrategy;
    }

    /**
     * Method for getting the encrypted message and returning it in plaintext
     * @return the message in cleartext
     */
    public String getMessage(){
        // Turn space separated hex pairs into a byte array
        byte[] bytes = computeHexToBytes(media.toString());

        // Decrypt the byte array and return it as a string
        bytes = encryptor.decrypt(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }

    /**
     * Method for encrypting a message and saving it to media
     * @param msg the message to encrypt
     */
    public void setMessage(String msg){
        // Refresh the media object
        media = new Media();

        // Turn the message to a byte array and encrypt it, printing the results
        media.set(encryptor.encrypt(msg.getBytes(StandardCharsets.UTF_8)));
        System.out.println(media.toString());
    }

    /**
     * Helper method for turning space separated hex pairs into a byte array
     * @param msg the message to convert
     * @return a byte array
     */
    private byte[] computeHexToBytes(String msg) {
        // Split the string into its constituent characters, instantiate the corresponding byte array
        String[] letters = msg.split(" ");
        byte[] bytes = new byte[letters.length];

        // For each hex pair, convert it into a character, then save it as a byte
        for (int i = 0; i < letters.length; i++){
            bytes[i] = (byte) ((Character.digit(letters[i].charAt(0), 16) << 4)
                    + Character.digit(letters[i].charAt(1), 16));
        }

        return bytes;
    }
}
