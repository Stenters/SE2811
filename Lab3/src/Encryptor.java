/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 3: Strategy-based Encryption
 * Author:     Stuart Enters
 * Date:       12/18/2018
 */

/**
 * Interface for defining the commonalities of encryptors
 */
public interface Encryptor {

    /**
     * All encryptors must be able to encrypt a message and return the result
     * @param msg the message to encrypt
     * @return the encrypted message
     */
    byte[] encrypt(byte[] msg);

    /**
     * All encryptors must be able to decrypt a message and return the result
     * @param msg the message to decrypt
     * @return the decrypted message
     */
    byte[] decrypt(byte[] msg);

}
