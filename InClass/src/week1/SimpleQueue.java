package week1;

import java.util.ArrayList;

public class SimpleQueue {
    private ArrayList<String> items = new ArrayList<>();

    public boolean isEmpty() {
        return items.size() == 0;
    }

    public void add(String nextItem) {
        items.add(nextItem);
    }

    public String get() {
        if ( items.size() == 0 )
            return null;
        else
            return items.remove(0);
    }

    public static void main(String[] args) {
        SimpleQueue todo = new SimpleQueue();
        todo.add("this");
        if ( todo.isEmpty() )
            System.out.println("Expected non-empty list.");
        todo.add("that");
        todo.add(".");
        if ( ! todo.get().equals("this") )
            System.out.println("Expected `this'");
        if ( ! todo.get().equals("that") )
            System.out.println("Expected `that'");
        if ( ! todo.get().equals(".") )
            System.out.println("Expected `.'");
        if ( ! todo.isEmpty() )
            System.out.println("Expected empty list.");
        System.out.println("All tests passed.");
    }
}