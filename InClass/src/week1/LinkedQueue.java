package week1;

import java.util.ArrayList;

public class LinkedQueue {
    private ArrayList<String> items = new ArrayList<>();

    public boolean isEmpty() {
        return items.size() == 0;
    }

    public void add(String nextItem) {
        items.add(nextItem);
    }

    public String get() {
        if ( items.size() == 0 )
            return null;
        else
            return items.remove(0);
    }

    public static void main(String[] args) {
        SimpleLinkedList<String> todo = new SimpleLinkedList<>();
        todo.addFirst("this");
        if ( todo.isEmpty() )
            System.out.println("Expected non-empty list.");
        todo.addLast("that");
        todo.addLast(".");
        if ( ! todo.first().equals("this") )
            System.out.println("Expected `this'");
        if ( ! todo.first().equals("that") )
            System.out.println("Expected `that'");
        if ( ! todo.first().equals(".") )
            System.out.println("Expected `.'");
        if ( ! todo.isEmpty() )
            System.out.println("Expected empty list.");
        System.out.println("All tests passed.");
    }
}