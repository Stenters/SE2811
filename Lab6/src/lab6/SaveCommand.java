package lab6;

public class SaveCommand extends CalculatorCommand {
  private String display;
  private String accumulator;
  private String memory;
  private Boolean newNumber;

  public SaveCommand(Calculator c) {
    super(c);
  }

  public void execute() {
    if (display == null){
      display = calculator.getDisplay();
      memory = calculator.getMemory();
      accumulator = calculator.getAccumulator();
      newNumber = calculator.getNewNumber();
    } else {
      calculator.setState(display, memory, accumulator, newNumber);
    }


    calculator.saveToMemory();
  }

  public void unexecute() {
    calculator.setState(display, memory, accumulator, newNumber);
  }
}
