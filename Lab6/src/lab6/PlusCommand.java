package lab6;

public class PlusCommand extends CalculatorCommand {
  private String display;
  private String accumulator;
  private Boolean newNumber;

  public PlusCommand(Calculator c) {
    super(c);
  }

  public void execute() {
    if (display == null){
      display = calculator.getDisplay();
      accumulator = calculator.getAccumulator();
      newNumber = calculator.getNewNumber();
    } else {
      calculator.setState(display, calculator.getMemory(), accumulator, newNumber);
    }

    calculator.plus();
  }

  public void unexecute() {
    calculator.setState(display, calculator.getMemory(), accumulator, newNumber);
  }
}
