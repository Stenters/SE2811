package lab6;

public class AppendDigitCommand extends CalculatorCommand {
  private String display;
  private boolean newNumber;

  public AppendDigitCommand(Calculator c) {
    super(c);
  }

  public void execute() {
    this.display = calculator.getDisplay();
    this.newNumber = calculator.getNewNumber();
  }

  public void unexecute() { calculator.setState(display, calculator.getMemory(), calculator.getAccumulator(), newNumber);
  }
}
