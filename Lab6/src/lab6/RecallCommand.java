package lab6;

public class RecallCommand extends CalculatorCommand {
  private String display;
  private String accumulator;
  private String memory;
  private Boolean newNumber;

  public RecallCommand(Calculator c) {
    super(c);
  }

  public void execute() {
    if (display == null){
      display = calculator.getDisplay();
      accumulator = calculator.getAccumulator();
      newNumber = calculator.getNewNumber();
    } else {
      calculator.setState(display, calculator.getMemory(), accumulator, newNumber);
    }

    calculator.recallFromMemory();
  }

  public void unexecute() {
    calculator.setState(display, memory, accumulator, newNumber);
  }
}
