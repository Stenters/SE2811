package lab6;

public class EnterCommand extends CalculatorCommand {
  private String display;
  private String accumulator;
  private Boolean newNumber;

  public EnterCommand(Calculator c) {
    super(c);
  }

  public void execute() {
    if (display == null){
      display = calculator.getDisplay();
      accumulator = calculator.getAccumulator();
      newNumber = calculator.getNewNumber();
    } else {
      calculator.setState(display, calculator.getMemory(), accumulator, newNumber);
    }

    calculator.enter();
  }

  public void unexecute() {
    calculator.setState(display, calculator.getMemory(), accumulator, newNumber);
  }
}
