import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.print("Enter an integer: ");
        Scanner in = new Scanner(System.in);

        while (!in.hasNextInt()){
            in.nextLine();
        }

        int x = in.nextInt();

        int y = x*x*x*x;

        System.out.println(x + " ** 4 = " + y);
    }
}
