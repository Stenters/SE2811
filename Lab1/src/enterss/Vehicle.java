/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;

/**
 * Interface for defining all the commonalities of a vehicle
 * @author enterss
 * @version 1.0
 */
public interface Vehicle {
    /**
     * Method for going backward in a vehicle
     * @param speed the speed to go backwards
     * @param accel the amount to accelerate
     */
    public void goBackward(double speed, double accel);

    /**
     * Method for going forward in a vehicle
     * @param speed the speed to go forward
     * @param accel the amount to accelerate
     */
    public void goForward(double speed, double accel);

    /**
     * Method for starting a vehicle
     * @return if it starts correctly
     */
    public boolean start();

    /**
     * Method for stopping a vehicle
     */
    public void stop();

}