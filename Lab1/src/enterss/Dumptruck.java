/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;


/**
 * Class for defining the functions of a Dumptruck
 * @author enterss
 * @version 1.0
 */
public class Dumptruck extends Pickup {

    private double plowWidth;

    public Dumptruck(int numWheels, double weight, String name, double loadCapacity){
        super(numWheels, weight, name, loadCapacity);
    }

    public Dumptruck(int numWheels, double weight, String name, double loadCapacity, double plowWidth){
        super(numWheels, weight, name, loadCapacity);
        this.plowWidth = plowWidth;
    }

    /**
     * Method for lowering the load of a Dumptruck
     */
    public void lowerLoad(){
        System.out.println( super.getName() + " is lowering the load");
    }

    /**
     * Method for raising the load of a Dumptruck
     */
    public void raiseLoad(){
        System.out.println( super.getName() + " is raising the load");
    }

    /**
     * Getter for the width of a plow
     * @return the width of the plow
     */
    @Override
    public double getPlowWidth() {
        return plowWidth;
    }

}