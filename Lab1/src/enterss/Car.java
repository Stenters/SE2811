/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;

/**
 * Class for defining how a car operates
 * @author enterss
 * @version 1.0
 */
public class Car implements Vehicle {

    private String name;
    private int numWheels;
    private double weight;

    /**
     * Constructor for a car
     * @param numWheels the number of wheels it has
     * @param weight the weight of the car
     * @param name the name of the car
     */
    public Car(int numWheels, double weight, String name){
        this.numWheels = numWheels;
        this.weight = weight;
        this.name = name;
    }

	/**
	 * Method for defining how a car goes backwards
	 * @param speed the speed to go backwards in mph
	 * @param accel the acceleration of the car in mph/s
	 */
	@Override
	public void goBackward(double speed, double accel){
        System.out.println( getName() + " is going backwards at " + speed +
                "mph, and accelerating at " + accel + "mph/s");
	}

	/**
	 * Method for defining how a car goes forward
	 * @param speed the speed to go backwards in mph
	 * @param accel the acceleration of the car in mph/s
	 */
	public void goForward(double speed, double accel){
        System.out.println( getName() + " is going forwards at " + speed +
                "mph, and accelerating at " + accel + "mph/s");
	}

    /**
     * Method for defining how a car starts
     * @return if it starts
     */
    public boolean start(){
        return true;
    }

    /**
     * Method for stopping a car
     */
    public void stop(){

    }

    /**
     * Getter for the name of a car
     * @return the name of the car
     */
    public String getName(){
        return name;
    }

    /**
     * Getter for the number of wheels of the car
     * @return the number of wheels of the car
     */
    public int getNumWheels(){
        return numWheels;
    }

    /**
     * Getter for the weight of the car
     * @return the weight of the car
     */
    public double getWeight(){
        return weight;
    }

}