/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;

/**
 * Class for defining the functions of a convertable
 * @author enterss
 * @version 1.0
 */
public class Convertable extends Car implements Plow {

	private double plowWidth;

	public Convertable(int numWheels, double weight, String name){
	    super(numWheels, weight, name);
    }

    public Convertable(int numWheels, double weight, String name, double plowWidth){
        super(numWheels, weight, name);
        this.plowWidth = plowWidth;
    }

    /**
     * Method for defining how a convertible lowers its roof
     */
    public void lowerRoof(){
        System.out.println( super.getName() + " is lowering its roof");
    }

    /**
     * Method for defining how a convertible raises its roof
     */
    public void raiseRoof(){
        System.out.println( super.getName() + " is raising its roof");
    }

    /**
     * Method for defining how a convertible lowers a plow (if it has one)
     */
    @Override
    public void lowerPlow(){
        System.out.println( super.getName() + " is lowering its plow");
    }

    /**
     * Method for defining how a convertible raises a plow (if it has one)
     */
    @Override
    public void raisePlow(){
        System.out.println( super.getName() + " is raising its plow");
    }

    /**
     * Getter for the width of a plow
     * @return the width of the plow
     */
    @Override
    public double getPlowWidth() {
        return plowWidth;
    }



}