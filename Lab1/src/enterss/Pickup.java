/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;

/**
 * Class for defining the functions of a pickup
 * @author enterss
 * @version 1.0
 */
public class Pickup extends Car implements Plow{

    private double loadCapacity;
    private double plowWidth;

    public Pickup(int numWheels, double weight, String name, double loadCapacity){
        super(numWheels, weight, name);
        this.loadCapacity = loadCapacity;
    }

    public Pickup(int numWheels, double weight, String name, double loadCapacity, double plowWidth){
        super(numWheels, weight, name);
        this.loadCapacity = loadCapacity;
        this.plowWidth = plowWidth;
    }

    /**
     * Getter for the load capacity of a pickup
     * @return the load capacity
     */
    public double getLoadCapacity(){
        return loadCapacity;
    }

    /**
     * Method for defining how a pickup lowers a plow (if it has one)
     */
    @Override
    public void lowerPlow(){
        System.out.println( super.getName() + " is lowering its plow");
    }

    /**
     * Method for defining how a pickup raises a plow (if it has one)
     */
    @Override
    public void raisePlow(){
        System.out.println( super.getName() + " is raising its plow");
    }

    /**
     * Getter for the width of a plow
     * @return the width of the plow
     */
    @Override
    public double getPlowWidth() {
        return plowWidth;
    }

}