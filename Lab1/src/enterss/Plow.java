/*
 * Course:     SE 2811
 * Term:       Winter 2018-19
 * Assignment: Lab 1 - Inheritance
 * Author:     Stuart Enters
 * Date:       11/27/18
 */
package enterss;

/**
 * Interface for defining the commonalities of vehicles with snow plows
 * @author enterss
 * @version 1.0
 */
public interface Plow {

    /**
     * Method for defining how a vehicle lowers its plow
     */
    public void lowerPlow();

    /**
     * Method for defining how a vehicle raises its plow
     */
    public void raisePlow();

    public double getPlowWidth();

}