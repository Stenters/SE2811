import enterss.Car;
import enterss.Convertable;
import enterss.Dumptruck;
import enterss.Pickup;

public class VehicleApp {

    public static void main(String[] args) {
        Car chad = new Car(4,500, "Chad the car");
        chad.goForward(40,4);
        chad.goBackward(40,4);
        System.out.println();

//        chad.getPlowWidth();
//        chad.lowerLoad();
//        chad.raiseRoof;

        Convertable conv = new Convertable(4,300, "Conv the convertable");
        conv.goForward(50, 4);
        conv.raiseRoof();
        conv.lowerRoof();
        System.out.println();

//        conv.raiseLoad();

        Pickup pike = new Pickup(6, 1500, "Pike the pickup", 500, 6);
        System.out.println(pike.getPlowWidth());
        pike.lowerPlow();
        pike.raisePlow();
        pike.goBackward(50, 4);
        System.out.println();

//        pike.raiseLoad();

        Dumptruck dan = new Dumptruck(8, 2500, "Dan the dumptruck",
                1000, 10);
        System.out.println(dan.getPlowWidth());
        dan.goForward(35, 1);
        dan.goBackward(35, 1);
        dan.raiseLoad();
        dan.lowerLoad();
        System.out.println();


    }
}
